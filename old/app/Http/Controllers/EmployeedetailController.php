<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\Model;
use App\Employeedetail;
use Illuminate\Http\Request;
use Auth;

class EmployeedetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth');
     }
    public function index()
    {
      $data = Employeedetail::get();
return view('/emplist',['empdata'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    $path=time().'.'.$request->img->getClientOriginalExtension();
    $request->img->move(public_path('Images'),$path);
              $emp = new Employeedetail;
               $emp->fname = $request->fname;
               $emp->lname = $request->lname;
               $emp->phone = $request->email;
               $emp->email = $request->phone;
               $emp->address = $request->address;
               $emp->pic = $path;
               $emp->save();
              return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employeedetail  $employeedetail
     * @return \Illuminate\Http\Response
     */
    public function show(Employeedetail $employeedetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employeedetail  $employeedetail
     * @return \Illuminate\Http\Response
     */
    public function edit(Employeedetail $employeedetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employeedetail  $employeedetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employeedetail $employeedetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employeedetail  $employeedetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employeedetail $employeedetail)
    {

    }
}
