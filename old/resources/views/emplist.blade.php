@extends('layouts.app')
@section('content')

<div class="container">
<div class="table-responsive">
  <table class="table table-dark">
        <thead>
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>PHONE</th>
                <th>Address</th>
                <th>Image</th>

            </tr>
        </thead>
        <tbody>
          @foreach($empdata as $data)

            <tr>
                <td>{{$data->id}}</td>
                <td>{{$data->fname}}</td>
                <td>{{$data->lname}}</td>
                <td>{{$data->email}}</td>
                <td>{{$data->phone}}</td>
                <td>{{$data->address}}</td>
                <td><img src="public/Images/{{$data->pic}}"></image></td>
                <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal{{$data->id}}">
                    View
                  </button>
                </td>
            </tr>
            @endforeach


        </tbody>
    </table>
</div>
</div>
@endsection
