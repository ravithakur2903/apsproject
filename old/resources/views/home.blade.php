@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



<div class="container">


  <form action="emp" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}
  <div class="input-group">
    <span class="input-group-addon">First Name</span>
    <input id="fname" type="text" class="form-control" name="fname" placeholder="fname">
  </div>
  <br>

  <div class="input-group">
    <span class="input-group-addon">Last Name</span>
    <input id="lname" type="text" class="form-control" name="lname" placeholder="lname">
  </div>
  <br>
  <div class="input-group">
    <span class="input-group-addon">Email id</span>
    <input id="email" type="text" class="form-control" name="email" placeholder="email">
  </div>
  <br>
  <div class="input-group">
    <span class="input-group-addon">Phone number</span>
    <input id="phone" type="text" class="form-control" name="phone" placeholder="Additional Info">
  </div>
  <br>
  <div class="input-group">
    <span class="input-group-addon">Address</span>
    <input id="address" type="text" class="form-control" name="address" placeholder="Additional Info">
  </div>

  <br>
  Select images: <input type="file" id="pic" name="img" multiple>

  <br>

  <input type="submit">


</form>



</div>
@endsection
